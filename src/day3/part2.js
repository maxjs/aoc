import { map } from './map.js';

const tree = '#';
const slopes = [{ row: 1, col: 1 }, { row: 1, col: 3 }, { row: 1, col: 5 }, { row: 1, col: 7 }, { row: 2, col: 1 }];
const result = slopes.reduce((acc, slope) => {
  let treeCount = 0;
  let col = 0;
  let row = 0;
  while (row < map.length) {
    while (col >= map[row].length) {
      map[row] += map[row];
    }
    const charAtCurrent = map[row][col];
    if (charAtCurrent === tree) {
      treeCount += 1;
    }
    row += slope.row;
    col += slope.col;
  }
  console.log(treeCount);

  return acc * treeCount;
}, 1);

console.log(`multiplied number of trees hit: ${result}`);
