import { passwords } from './passwords.js';

const validPasswords = passwords.reduce((acc, current) => {
  const pattern = /(\d+?)-(\d+?) (\w): (\w+)/;
  const match = current.match(pattern);
  const lowerBound = match[1];
  const higherBound = match[2];
  const letter = match[3];
  const word = match[4];
  const occurences = [...word].reduce((counter, currentLetter) => {
    if (currentLetter === letter) {
      return counter + 1;
    }
    return counter;
  }, 0);
  const isValid = lowerBound <= occurences && occurences <= higherBound;
  return acc + (isValid ? 1 : 0);
}, 0);

console.log(`valid passwords: ${validPasswords}`);
