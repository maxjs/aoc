import { passwords } from './passwords.js';

const validPasswords = passwords.reduce((acc, current) => {
  const pattern = /(\d+?)-(\d+?) (\w): (\w+)/;
  const match = current.match(pattern);
  const lowerBound = match[1];
  const higherBound = match[2];
  const letter = match[3];
  const word = match[4];
  const firstChar = word[lowerBound - 1];
  const secondChar = word[higherBound - 1];
  const isValid = (firstChar === letter && secondChar !== letter) || (firstChar !== letter && secondChar === letter);
  return acc + (isValid ? 1 : 0);
}, 0);

console.log(`valid passwords: ${validPasswords}`);
