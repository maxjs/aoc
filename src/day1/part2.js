import { numbers } from './numbers.js';

numbers.forEach((number1) => numbers.forEach((number2) => numbers.forEach((number3) => {
  if (number1 + number2 + number3 === 2020) {
    console.log(`The answer is: ${number1 * number2 * number3}`);
    process.exit(0);
  }
})));
