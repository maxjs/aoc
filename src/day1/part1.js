import { numbers } from './numbers.js';

numbers.forEach((number1) => numbers.forEach((number2) => {
  if (number1 + number2 === 2020) {
    console.log(`The answer is: ${number1 * number2}`);
    process.exit(0);
  }
}));
